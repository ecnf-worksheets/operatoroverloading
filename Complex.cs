﻿using System;

namespace OperatorOverloading
{
    public struct Complex
    {
        public Complex(double re, double im)
        {
            this.Re = re;
            this.Im = im;
        }

        public double Re { get; }
        public double Im { get; }

        public override string ToString() { return String.Format("({0,5:0.0},{1,5:0.0}i)", Re, Im); }

        public override int GetHashCode()
        {
            return Convert.ToInt32(this.Re * this.Im * 1000);
        }
    }
}
